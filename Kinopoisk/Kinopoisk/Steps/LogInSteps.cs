﻿using Kinopoisk.Pages;
using OpenQA.Selenium;

namespace Kinopoisk
{
    class LogInSteps
    {
        private IWebDriver _driver;
        private UnLogedPage _unLogedPage;
        private LoginAuthorizationPage _loginAuthorization;
        private PasswordAuthorizationPage _passwordAuthorization;

        public LogInSteps(IWebDriver driver)
        {
            _driver = driver;
            _unLogedPage = new UnLogedPage(_driver);
            _loginAuthorization = new LoginAuthorizationPage(_driver);
            _passwordAuthorization = new PasswordAuthorizationPage(_driver);
        }

        public void MainPageLogInButtonClick()
        {
            _unLogedPage.ClickLogInButton();
        }

        public void LoginInput(string login)
        {
            _loginAuthorization.EnterUserLogin(login);
            _loginAuthorization.ClickLogInButton();
        }

        public bool IsLoginAuthorizationSuccessful()
        {
            return _loginAuthorization.HasLoginAuthorizationPassed();
        }

        public bool IncorrectLoginMessageAppeared()
        {
            return _loginAuthorization.HasLoginErrorMessageAppeared();
        }

        public void PasswordInput(string password)
        {
            _passwordAuthorization.EnterUserPassword(password);
            _passwordAuthorization.ClickLogInButton();
        }

        public bool IsPasswordAuthorizationSuccessful()
        {
            return _passwordAuthorization.HasPassworedAuthorizationPassed();
        }

        public bool IncorrectPasswordMessageAppeared()
        {
            return _passwordAuthorization.HasPasswordErrorMessageAppeared();
        }
    }
}
