﻿using Kinopoisk.Pages;
using OpenQA.Selenium;

namespace Kinopoisk.Steps
{
    class MoviePageInteractionsSteps
    {
        private IWebDriver _driver;
        private MoviePage _moviePage;
        private UserMovies _userMovies;
        private WatchedMovesPage _watchedMoves;

        public MoviePageInteractionsSteps(IWebDriver driver)
        {
            _driver = driver;
            _moviePage = new MoviePage(_driver);
            _userMovies = new UserMovies(_driver);
            _watchedMoves = new WatchedMovesPage(_driver);
        }

        public bool WatchTrailer()
        {
            _moviePage.ClickTrailerButton();
            return _moviePage.HasTrailerAppeared();
        }

        public bool MakeNotice(string notice)
        {
            _moviePage.ClickNoticeButton();
            _moviePage.NoticeInput(notice);
            return _moviePage.IsNoticeInputSuccessful(notice);
        }

        public void RemoveNotice()
        {
            _moviePage.RemoveNotice();
        }

        public bool AddToFavouriteMovies(string movieName)
        {
            if (_moviePage.IsFavouriteButtonActive(true))
            {
                _moviePage.ClickFavouriteButton();
                _moviePage.ClickWatchedButton();
            }
            if (_moviePage.IsFavouriteButtonActive(false)) _moviePage.ClickFavouriteButton();
            _moviePage.ClickFavouriteMoviesFolderLink();
            return _userMovies.IsMovieAddedToFavouriteMoviesFolder(movieName);
        }

        public bool AddToWantToWatchMovies(string movieName)
        {
            if (_moviePage.IsWantToWatchButtonActive(true)) _moviePage.ClickWantToWatchButton();
            if (_moviePage.IsWantToWatchButtonActive(false)) _moviePage.ClickWantToWatchButton();
            _moviePage.ClickWantToWatchFolderLink();
            return _userMovies.IsMovieAddedToWantToWatchFolder(movieName);
        }

        public bool AddToWatchedMovies(string movieName, string genreFolder)
        {
            if (_moviePage.IsWatchedButtonActive(true)) _moviePage.ClickWatchedButton();
            if (_moviePage.IsWatchedButtonActive(false)) _moviePage.ClickWatchedButton();
            if (_moviePage.IsWatchedButtonActive(true)) _moviePage.ClickWatchedMoviesFolderLink();
            return _watchedMoves.IsMovieWatched(movieName, genreFolder);
        }
    }
}
