﻿using Kinopoisk.Pages;
using OpenQA.Selenium;

namespace Kinopoisk.Steps
{
    class SearchSteps
    {
        private static IWebDriver _driver;
        private MainPage _mainPage;
        private AdvancedSearchPage _advancedSearch;

        private MoviePage _moviePage;

        public SearchSteps(IWebDriver driver)
        {
            _driver = driver;
            _mainPage = new MainPage(_driver);
            _advancedSearch = new AdvancedSearchPage(_driver);
            _moviePage = new MoviePage(_driver);
        }

        public static IWebDriver GetDriver()
        {
            return _driver;
        }
        public void SearchByMovieName(string movieName)
        {
            _mainPage.InputInSearchField(movieName);
            _mainPage.ClickSearchSubmitButton();
        }

        public bool IsSimpleSearchSuccessful(string movieName)
        {
            return _moviePage.IsSearchSuccessful(movieName);
        }

        public string AdvancedSearchByCountry()
        {
            _mainPage.ClickAdvanceSearchButton();
            _advancedSearch.ClickCountryDropdownMenu();
            string searchParameter = _advancedSearch.ChooseCountry();
            _advancedSearch.ClickSearchButton();
            return searchParameter;
        }

        public bool IsAdvancedSearchSuccessful(string searchParameter)
        {
            return _advancedSearch.IsSearchSuccessful(searchParameter);
        }
    }
}
