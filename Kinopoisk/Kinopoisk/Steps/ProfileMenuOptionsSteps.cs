﻿using Kinopoisk.Pages;
using OpenQA.Selenium;

namespace Kinopoisk.Steps
{
    class ProfileMenuOptionsSteps
    {
        private IWebDriver _driver;
        private LogedInPage _logedInPage;
        private UserProfileEditPage _profileEditPage;

        public ProfileMenuOptionsSteps(IWebDriver driver)
        {
            _driver = driver;
            _logedInPage = new LogedInPage(_driver);
            _profileEditPage = new UserProfileEditPage(_driver);
        }

        public void MoveMousePointerToUserAvatar()
        {
            _logedInPage.MoveMousePointerToUserAvatar();
        }

        public bool IsMoveMousePointerMovedToUserAvatar()
        {
            return _logedInPage.IsMousePointerMovedToUserAvatar();
        }

        public void ClickLogOutOption()
        {
            _logedInPage.ClickUserLogOutLink();
        }

        public bool IsLogOutSuccessful()
        {
            return _logedInPage.IsUserLogOutLinkClicked();
        }

        public string ChangeProfileSettings()
        {
            _logedInPage.ClickUserSettingsLink();
            _profileEditPage.ClickCountryDropdownMenu();
            string settingChangeValue = _profileEditPage.ChooseCountry();
            _profileEditPage.ClickSaveAllButton();
            return settingChangeValue;
        }

        public bool IsProfileSettingsChangeSuccessful(string settingChange)
        {
            _profileEditPage.MoveMousePointerToUserAvatar();
            _profileEditPage.ClickUserAccountLink();
            return _profileEditPage.IsSettingsChangeSuccessful(settingChange);
        }
    }
}
