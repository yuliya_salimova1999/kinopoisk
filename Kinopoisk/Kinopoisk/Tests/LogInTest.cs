﻿using NUnit.Framework;

namespace Kinopoisk.Tests
{
    class LogInTest : BaseTest
    {
        private LogInSteps _logInSteps => new LogInSteps(Driver);
        private const string _correctLogin = "yuliya-salimova-1999";
        private const string _correctPassword = "8278146";
        private const string _incorrectLogin = "hdsghgfsgfjgjshgdfjdhsg";
        private const string _incorrectPassword = "8278147";

        [Test]
        public void SuccessfulAuthorization()
        {
            _logInSteps.MainPageLogInButtonClick();
            _logInSteps.LoginInput(_correctLogin);
            Assert.IsTrue(_logInSteps.IsLoginAuthorizationSuccessful());
            _logInSteps.PasswordInput(_correctPassword);
            Assert.IsTrue(_logInSteps.IsPasswordAuthorizationSuccessful());
        }

        [Test]
        public void FailedLoginAuthorization()
        {
            _logInSteps.MainPageLogInButtonClick();
            _logInSteps.LoginInput(_incorrectLogin);
            Assert.IsTrue(_logInSteps.IncorrectLoginMessageAppeared());
        }

        [Test]
        public void FailedPasswordAuthorization()
        {
            _logInSteps.MainPageLogInButtonClick();
            _logInSteps.LoginInput(_correctLogin);
            Assert.IsTrue(_logInSteps.IsLoginAuthorizationSuccessful());
            _logInSteps.PasswordInput(_incorrectPassword);
            Assert.IsTrue(_logInSteps.IncorrectPasswordMessageAppeared());
        }
    }
}