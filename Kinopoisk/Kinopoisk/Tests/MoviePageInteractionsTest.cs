﻿using Kinopoisk.Steps;
using NUnit.Framework;

namespace Kinopoisk.Tests
{
    class MoviePageInteractionsTest : BaseTest
    {
        private SearchSteps _searchSteps => new SearchSteps(Driver);
        private LogInSteps _logInSteps => new LogInSteps(Driver);
        private MoviePageInteractionsSteps _interactionsSteps => new MoviePageInteractionsSteps(Driver);
        private const string _correctLogin = "yuliya-salimova-1999";
        private const string _correctPassword = "8278146";
        private const string _filmName = "RoboCop";
        private const string _genreFolder = "боевик";
        private const string _noticeText = "Nice film!";

        [Test]
        public void WatchTrailerOption()
        {
            _searchSteps.SearchByMovieName(_filmName);
            Assert.IsTrue(_interactionsSteps.WatchTrailer());
        }

        [Test]
        public void MakingNoticeOption()
        {
            _logInSteps.MainPageLogInButtonClick();
            _logInSteps.LoginInput(_correctLogin);
            _logInSteps.PasswordInput(_correctPassword);
            _searchSteps.SearchByMovieName(_filmName);
            Assert.IsTrue(_interactionsSteps.MakeNotice(_noticeText));
            _interactionsSteps.RemoveNotice();
        }

        [Test]
        public void AddToWantToWatchOption()
        {
            _logInSteps.MainPageLogInButtonClick();
            _logInSteps.LoginInput(_correctLogin);
            _logInSteps.PasswordInput(_correctPassword);
            _searchSteps.SearchByMovieName(_filmName);
            Assert.IsTrue(_interactionsSteps.AddToWantToWatchMovies(_filmName));
        }

        [Test]
        public void AddToWatchedMoviesOption()
        {
            _logInSteps.MainPageLogInButtonClick();
            _logInSteps.LoginInput(_correctLogin);
            _logInSteps.PasswordInput(_correctPassword);
            _searchSteps.SearchByMovieName(_filmName);
            Assert.IsTrue(_interactionsSteps.AddToWatchedMovies(_filmName, _genreFolder));
        }

        [Test]
        public void AddToFavouriteMoviesOption()
        {
            _logInSteps.MainPageLogInButtonClick();
            _logInSteps.LoginInput(_correctLogin);
            _logInSteps.PasswordInput(_correctPassword);
            _searchSteps.SearchByMovieName(_filmName);
            Assert.IsTrue(_interactionsSteps.AddToFavouriteMovies(_filmName));
        }
    }
}
