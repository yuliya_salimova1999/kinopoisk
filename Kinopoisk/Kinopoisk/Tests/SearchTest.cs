﻿using Kinopoisk.Steps;
using NUnit.Framework;

namespace Kinopoisk.Tests
{
    class SearchTest : BaseTest
    {
        private SearchSteps _searchSteps => new SearchSteps(Driver);
        private const string _filmName = "Avatar";

        [Test]
        public void SimpleSearch()
        {
            _searchSteps.SearchByMovieName(_filmName);
            Assert.IsTrue(_searchSteps.IsSimpleSearchSuccessful(_filmName));
        }

        [Test]
        public void AdvancedSearch()
        {
            Assert.IsTrue(_searchSteps.IsAdvancedSearchSuccessful(_searchSteps.AdvancedSearchByCountry()));
        }
    }
}
