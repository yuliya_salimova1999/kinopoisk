﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace Kinopoisk.Tests
{
    abstract class BaseTest
    {
        protected IWebDriver Driver;
        private const string _mainPageUrl = "https://www.kinopoisk.ru/";

        [SetUp]
        public void Setup()
        {
            Driver = new ChromeDriver();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(240);
            Driver.Manage().Window.Maximize();
            Console.Out.WriteLine(Driver.Manage().Window.Size);
            Driver.Navigate().GoToUrl(_mainPageUrl);
        }

        [TearDown]
        public void Dispose()
        {
            Driver.Quit();
        }
    }
}
