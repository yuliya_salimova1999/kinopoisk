﻿using Kinopoisk.Steps;
using NUnit.Framework;

namespace Kinopoisk.Tests
{
    class ProfileMenuOptionsTest : BaseTest
    {
        private LogInSteps _logInSteps => new LogInSteps(Driver);
        private ProfileMenuOptionsSteps _profileMenuOptionsSteps => new ProfileMenuOptionsSteps(Driver);
        private const string _correctLogin = "yuliya-salimova-1999";
        private const string _correctPassword = "8278146";

        [Test]
        public void LogOutSuccessful()
        {
            _logInSteps.MainPageLogInButtonClick();
            _logInSteps.LoginInput(_correctLogin);
            _logInSteps.PasswordInput(_correctPassword);
            _profileMenuOptionsSteps.MoveMousePointerToUserAvatar();
            Assert.IsTrue(_profileMenuOptionsSteps.IsMoveMousePointerMovedToUserAvatar());
            _profileMenuOptionsSteps.ClickLogOutOption();
            Assert.IsTrue(_profileMenuOptionsSteps.IsLogOutSuccessful());
        }

        [Test]
        public void ProfileSettingsChangeSuccessful()
        {
            _logInSteps.MainPageLogInButtonClick();
            _logInSteps.LoginInput(_correctLogin);
            _logInSteps.PasswordInput(_correctPassword);
            _profileMenuOptionsSteps.MoveMousePointerToUserAvatar();
            Assert.IsTrue(_profileMenuOptionsSteps.IsMoveMousePointerMovedToUserAvatar());
            Assert.IsTrue(_profileMenuOptionsSteps.IsProfileSettingsChangeSuccessful(_profileMenuOptionsSteps.ChangeProfileSettings()));
        }
    }
}
