﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;

namespace Kinopoisk.Extensions
{
    public static class WebDriverExtension
    {
        private static WebDriverWait _wait;

        public static IWebElement GetElement(this IWebDriver webDriver, By locator) => Wait(webDriver).Until(driver => driver.FindElement(locator));

        public static ReadOnlyCollection<IWebElement> GetElements(this IWebDriver webDriver, By locator) => Wait(webDriver).Until(driver => driver.FindElements(locator));

        public static bool IsElementExists(this IWebDriver webDriver, By locator)
        {
            try
            {
                return Wait(webDriver).Until(driver => driver.FindElements(locator).Count > 0);
            }
            catch
            {
                return false;
            }
        }

        public static bool IsElementDispalyed(this IWebDriver webDriver, By locator) => Wait(webDriver).Until(driver => driver.FindElement(locator).Displayed);

        public static bool IsElementTextContains(this IWebDriver webDriver, By locator, string text) => Wait(webDriver).Until(driver => GetElement(webDriver, locator).Text.Contains(text));

        public static bool IsElementActive(this IWebDriver webDriver, By locator, bool isElementShouldBeActive)
        {
            try
            {
                return Wait(webDriver).Until(driver => GetElement(webDriver, locator).GetAttribute("class").Contains("act") == isElementShouldBeActive);
            }
            catch
            {
                return false;
            }

        }

        public static void JSScrollAndClick(this IWebDriver webDriver, By locator)
        {
            ((IJavaScriptExecutor)webDriver).ExecuteScript("window.scrollTo(0,document.body.scrollHeight)");
            new Actions(webDriver).MoveToElement(GetElement(webDriver, locator)).Build().Perform();
            GetElement(webDriver, locator).Click();
        }

        public static void Click(this IWebDriver webDriver, By locator) => Wait(webDriver).Until(driver => IsClicked(webDriver, locator) == true);

        private static bool IsClicked(this IWebDriver webDriver, By locator)
        {
            try
            {
                GetElement(webDriver, locator).Click();
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static WebDriverWait Wait(IWebDriver webDriver)
        {
            _wait = new WebDriverWait(webDriver,TimeSpan.FromSeconds(240));
            _wait.IgnoreExceptionTypes(typeof(StaleElementReferenceException));
            return _wait;
        }
    }
}
