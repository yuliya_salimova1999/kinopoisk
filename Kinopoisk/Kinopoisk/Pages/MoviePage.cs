﻿using OpenQA.Selenium;
using Kinopoisk.Extensions;
using OpenQA.Selenium.Interactions;

namespace Kinopoisk.Pages
{
    class MoviePage : BasePage
    {
        private IWebElement _filmHeadline => Driver.GetElement(By.ClassName("moviename-title-wrapper"));
        private IWebElement _fimAlternativeHeadline => Driver.GetElement(By.ClassName("alternativeHeadline"));
        private IWebElement _trailerButton => Driver.GetElement(By.Id("movie-trailer-button"));
        private IWebElement _favouriteButton => Driver.GetElement(By.Id("btn_fav_film"));
        private IWebElement _favouriteMoviesFolderLink => Driver.GetElement(By.XPath("//li[@id='ms_folder_6']/a"));
        private IWebElement _watchedButton => Driver.GetElement(By.Id("btn_null_vote"));
        //private IWebElement _watchedMoviesFolderLink => Driver.GetElement(By.XPath("//*[@class='el_2']/div[@class='link']/a"));
        private IWebElement _noticeButton => Driver.GetElement(By.Id("btn_film_notice"));
        private IWebElement _noticeInputField => Driver.GetElement(By.XPath("//div[@id='ta_film_notice']/form/textarea"));
        private IWebElement _noticeSaveButton => Driver.GetElement(By.XPath("//div[@id='ta_film_notice']/form/input[@class='save']"));
        private IWebElement _noticeRemoveButton => Driver.GetElement(By.XPath("//div[@id='txt_film_notice']//a[@class='remove']"));
        private IWebElement _wantToWatchButton => Driver.GetElement(By.XPath("//span[@onclick='FavFolderClick(3575)']"));
        //private IWebElement _wantToWatchFolderLink => Driver.GetElement(By.XPath("//li[@id='ms_folder_3575']/a"));

        private By _watchedMoviesFolderLinkLocator = By.XPath("//*[@class='el_2']/div[@class='link']/a");
        private By _wantToWatchFolderLinkLocator = By.XPath("//li[@id='ms_folder_3575']/a");
        private By _wantToWatchButtonLocator = By.XPath("//span[@onclick='FavFolderClick(3575)']");
        private By _watchedButtonLocator = By.Id("btn_null_vote");
        private By _favouriteButtonLocator = By.Id("btn_fav_film");
        private By _noticeTextLocator = By.XPath("//div[@id='txt_film_notice']/div[@class='descr']");
        private By _trailerPlaceholderLocator = By.ClassName("discovery-trailers-overlay");

        public MoviePage(IWebDriver driver) : base(driver) { }

        public void ClickTrailerButton() => _trailerButton.Click();

        public void ClickFavouriteButton() => _favouriteButton.Click();

        public bool IsFavouriteButtonActive(bool isElementShouldBeActive) => Driver.IsElementActive(_favouriteButtonLocator, isElementShouldBeActive);

        public void ClickFavouriteMoviesFolderLink() => _favouriteMoviesFolderLink.Click();

        public void ClickNoticeButton() => _noticeButton.Click();

        public void NoticeInput(string notice)
        {
            _noticeInputField.SendKeys(notice);
            _noticeSaveButton.Click();
        }

        public bool IsNoticeInputSuccessful(string notice) => Driver.IsElementTextContains(_noticeTextLocator, notice);

        public void RemoveNotice()
        {
            _noticeRemoveButton.Click();
            Driver.SwitchTo().Alert().Accept();
        }

        public void ClickWatchedButton() => _watchedButton.Click();

        public bool IsWatchedButtonActive(bool isElementShouldBeActive) => Driver.IsElementActive(_watchedButtonLocator, isElementShouldBeActive);

        public void ClickWatchedMoviesFolderLink()
        {
            new Actions(Driver).MoveToElement(Driver.GetElement(_watchedButtonLocator)).Build().Perform();
            Driver.Click(_watchedMoviesFolderLinkLocator);
        }

        public void ClickWantToWatchButton() => _wantToWatchButton.Click();

        public bool IsWantToWatchButtonActive(bool isElementShouldBeActive) => Driver.IsElementActive(_wantToWatchButtonLocator, isElementShouldBeActive);

        public void ClickWantToWatchFolderLink() => Driver.Click(_wantToWatchFolderLinkLocator);

        public bool HasTrailerAppeared() => Driver.IsElementExists(_trailerPlaceholderLocator);

        public bool IsSearchSuccessful(string filmName) => _filmHeadline.Text == filmName || _fimAlternativeHeadline.Text == filmName;
    }
}
