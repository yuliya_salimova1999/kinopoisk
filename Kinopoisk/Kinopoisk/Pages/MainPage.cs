﻿using OpenQA.Selenium;
using Kinopoisk.Extensions;

namespace Kinopoisk.Pages
{
    class MainPage : BasePage
    {
        protected IWebElement SearchField => Driver.GetElement(By.Name("kp_query"));
        protected IWebElement AdvancedSearchButton => Driver.GetElement(By.XPath("//a[@aria-label='advanced-search']"));

        protected By RecommendedFilm = By.XPath("//div[@data-index='0']/a");

        public MainPage(IWebDriver driver) : base(driver) { }

        public void InputInSearchField(string inputData) => SearchField.SendKeys(inputData);

        public void ClickAdvanceSearchButton() => AdvancedSearchButton.Click();

        public void ClickSearchSubmitButton() => Driver.JSScrollAndClick(RecommendedFilm);
    }
}
