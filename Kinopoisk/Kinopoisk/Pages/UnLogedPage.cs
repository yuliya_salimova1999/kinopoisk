﻿using OpenQA.Selenium;
using Kinopoisk.Extensions;

namespace Kinopoisk.Pages
{
    class UnLogedPage : BasePage
    {
        private IWebElement _logInButton => Driver.GetElement(By.XPath("//button[contains(@class, 'login')]"));
        private By _loginFieldLocator = By.ClassName("passp-form-field_text");

        public UnLogedPage(IWebDriver driver) : base(driver) { }

        public void ClickLogInButton() => _logInButton.Click();

        public bool IsLogInButtonClicked() => Driver.IsElementDispalyed(_loginFieldLocator);
    }
}
