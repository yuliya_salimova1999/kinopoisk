﻿using OpenQA.Selenium;

namespace Kinopoisk.Pages
{
    class BasePage
    {
        protected IWebDriver Driver;

        public BasePage(IWebDriver driver)
        {
            Driver = driver;
        }
    }
}
