﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using Kinopoisk.Extensions;
using System.Linq;

namespace Kinopoisk.Pages
{
    class UserProfileEditPage : LogedInPage
    {
        private IWebElement _countryDropdownMenu => Driver.GetElement(By.XPath("//select[contains(@name,'id_country')]"));
        private IReadOnlyCollection<IWebElement> _countryList => Driver.GetElements(By.XPath("//select[contains(@name,'country')]/option"));
        private IWebElement _saveAllButton => Driver.GetElement(By.Id("js-save-edit-form"));
        private IWebElement _changedCountryLabel => Driver.GetElement(By.XPath("//a[contains(@href,'country')]"));

        public UserProfileEditPage(IWebDriver driver) : base(driver) { }

        public void ClickCountryDropdownMenu() => _countryDropdownMenu.Click();

        public string ChooseCountry()
        {
            IWebElement _settingChange = _countryList.ToList()[new Random().Next(0, _countryList.Count)];
            _countryDropdownMenu.SendKeys(_settingChange.Text);
            return _settingChange.GetAttribute("value");
        }

        public void ClickSaveAllButton() => _saveAllButton.Click();

        public bool IsSettingsChangeSuccessful(string value) => _changedCountryLabel.GetAttribute("href").Contains(value);
    }
}
