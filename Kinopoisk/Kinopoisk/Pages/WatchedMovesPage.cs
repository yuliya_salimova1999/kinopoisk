﻿using OpenQA.Selenium;
using Kinopoisk.Extensions;

namespace Kinopoisk.Pages
{
    class WatchedMovesPage : BasePage
    {
        private By _watchedMovieNameLocator = By.XPath("//div[text()='1']/following-sibling::div[@class='info']/div[@class='nameRus']/a");
        private By _watchedMovieAlternativeNameLocator = By.XPath("//div[text()='1']/following-sibling::div[@class='info']/div[@class='nameEng']");
        private By _bigEyeIcon = By.XPath("//div[text()='1']/following-sibling::div[@class='selects vote_widget']//div[contains(@class,'bigEye')]");

        public WatchedMovesPage(IWebDriver driver) : base(driver) { }

        public bool IsMovieWatched(string movieName, string folderName)
        {
            Driver.GetElement(By.XPath($"//a[text()='{folderName}']")).Click();
            return (Driver.IsElementTextContains(_watchedMovieAlternativeNameLocator, movieName) ||
                Driver.IsElementTextContains(_watchedMovieNameLocator, movieName)) && Driver.IsElementExists(_bigEyeIcon);
        }
    }
}
