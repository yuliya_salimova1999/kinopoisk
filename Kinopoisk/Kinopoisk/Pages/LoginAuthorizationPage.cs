﻿using OpenQA.Selenium;
using Kinopoisk.Extensions;

namespace Kinopoisk.Pages
{
    class LoginAuthorizationPage : BasePage
    {
        private IWebElement _loginInputField => Driver.GetElement(By.Id("passp-field-login"));
        private IWebElement _logInButton => Driver.GetElement(By.XPath("//button[@type='submit']"));

        private By _loginFieldLocator = By.ClassName("passp-password-field");
        private By _loginErrorMessageLocator = By.ClassName("passp-form-field__error");

        public LoginAuthorizationPage(IWebDriver driver) : base(driver) { }

        public void EnterUserLogin(string login) => _loginInputField.SendKeys(login);

        public void ClickLogInButton() => _logInButton.Click();

        public bool HasLoginErrorMessageAppeared() => Driver.IsElementExists(_loginErrorMessageLocator);

        public bool HasLoginAuthorizationPassed() => Driver.IsElementExists(_loginFieldLocator);
    }
}
