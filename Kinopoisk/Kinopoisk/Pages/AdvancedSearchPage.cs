﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using Kinopoisk.Extensions;

namespace Kinopoisk.Pages
{
    class AdvancedSearchPage : BasePage
    {
        private IWebElement _countryDropdownMenu => Driver.GetElement(By.Id("country"));
        private IReadOnlyCollection<IWebElement> _countryList => Driver.GetElements(By.XPath("//select[@id='country']/option"));
        private IWebElement _searchButton => Driver.GetElement(By.XPath("//form[@name='film_search']/input[contains(@class,'18')]"));
        private IWebElement _posterName => Driver.GetElement(By.XPath("//span[@class='selection-poster__heading']"));

        public AdvancedSearchPage(IWebDriver driver) : base(driver) { }

        public void ClickCountryDropdownMenu() => _countryDropdownMenu.Click();

        public string ChooseCountry()
        {
            string _settingChange = _countryList.ToList()[new Random().Next(0, _countryList.Count)].Text;
            _countryDropdownMenu.SendKeys(_settingChange);
            return _settingChange;
        }

        public void ClickSearchButton() => _searchButton.Click();

        public bool IsSearchSuccessful(string value) => _posterName.Text == value;
    }
}
