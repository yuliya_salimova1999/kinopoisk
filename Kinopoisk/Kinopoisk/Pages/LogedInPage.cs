﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Kinopoisk.Extensions;

namespace Kinopoisk.Pages
{
    class LogedInPage : BasePage
    {
        protected IWebElement UserAvatar => Driver.GetElement(By.XPath("//div[@class='header-fresh-user-partial-component__button']/.."));
        protected By UserAccountLink = By.ClassName("user-account-link-partial-component__link");
        protected By UserSettingsLink = By.XPath("//a[@href='/mykp/edit_main/']");
        protected By UserLogOutLink = By.XPath("//a[contains(@href,'support') and contains(@class,'header')]/../following-sibling::li/a");
        protected By UserDropdownMenuLocator = By.XPath("//ul[contains(@class,'dropdown')]");
        private By _loginButtonLocator = By.XPath("//button[contains(@class, 'login')]");

        public LogedInPage(IWebDriver driver) : base(driver) { }

        public void MoveMousePointerToUserAvatar() => new Actions(Driver).MoveToElement(UserAvatar).Build().Perform();

        public bool IsMousePointerMovedToUserAvatar() => Driver.IsElementDispalyed(UserDropdownMenuLocator);

        public void ClickUserAccountLink() => Driver.JSScrollAndClick(UserAccountLink);

        public void ClickUserSettingsLink() => Driver.JSScrollAndClick(UserSettingsLink);

        public void ClickUserLogOutLink() => Driver.JSScrollAndClick(UserLogOutLink);

        public bool IsUserLogOutLinkClicked() => Driver.IsElementExists(_loginButtonLocator);
    }
}
