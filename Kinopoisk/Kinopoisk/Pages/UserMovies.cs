﻿using OpenQA.Selenium;
using Kinopoisk.Extensions;

namespace Kinopoisk.Pages
{
    class UserMovies : BasePage
    {
        private IWebElement _lastAddedMovieName => Driver.GetElement(By.XPath("//li[@class='item']/div[@class='number' and text()='1']/following-sibling::div[@class='info']/a"));
        private IWebElement _lastAddedMovieAlternativeName => Driver.GetElement(By.XPath("//li[@class='item']/div[@class='number' and text()='1']/following-sibling::" +
            "div[@class='info']/span[contains(@style,'margin')]/preceding-sibling::span"));
        private IWebElement _favouriteMoviesMenuOption => Driver.GetElement(By.Id("folder_6"));
        private IWebElement _wantToWatchMenuOption => Driver.GetElement(By.Id("folder_3575"));

        public UserMovies(IWebDriver driver) : base(driver) { }

        public bool IsMovieAddedToFavouriteMoviesFolder(string filmName) => _favouriteMoviesMenuOption.GetAttribute("class").Contains("act") &&
            (_lastAddedMovieName.Text == filmName || _lastAddedMovieAlternativeName.Text.Contains(filmName));

        public bool IsMovieAddedToWantToWatchFolder(string filmName) => _wantToWatchMenuOption.GetAttribute("class").Contains("act") &&
            (_lastAddedMovieName.Text == filmName || _lastAddedMovieAlternativeName.Text.Contains(filmName));
    }
}
