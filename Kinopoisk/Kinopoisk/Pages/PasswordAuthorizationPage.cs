﻿using OpenQA.Selenium;
using Kinopoisk.Extensions;

namespace Kinopoisk.Pages
{
    class PasswordAuthorizationPage : BasePage
    {
        private IWebElement _passwordInputField => Driver.GetElement(By.XPath("//*[@name='passwd']"));
        private IWebElement _logInButton => Driver.GetElement(By.XPath("//button[@type='submit']"));

        private By _passwordErrorMessageLocator = By.ClassName("passp-form-field__error");
        private By _userAvatarLocator = By.ClassName("header-fresh-user-partial-component__avatar");

        public PasswordAuthorizationPage(IWebDriver driver) : base(driver) { }

        public void EnterUserPassword(string password) => _passwordInputField.SendKeys(password);

        public void ClickLogInButton() => _logInButton.Click();

        public bool HasPasswordErrorMessageAppeared() => Driver.IsElementDispalyed(_passwordErrorMessageLocator);

        public bool HasPassworedAuthorizationPassed() => Driver.IsElementDispalyed(_userAvatarLocator);
    }
}
